//
//  ViewController.swift
//  Stopwatch
//
//  Created by Victor Hernandez on 11/24/14.
//  Copyright (c) 2014 Compubility. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timerText: UILabel!
    var mytime = 0;
    var timer = NSTimer();
    
    @IBAction func playPressed(sender: AnyObject) {

    timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("count"), userInfo: nil, repeats: true);
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
   
    }
    func timeFormat (var number:Int) -> String{
        if (number < 10){
            return "0\(number)"
        }
        else if (number < 60){
            return "\(number)"
        }
        else{
            return "00"
        }
    }

    func count(){
        mytime++;
        // Gracefully reset if over allotted time space
        if (mytime >= 5184000){
            mytime = 0
        }
        var milliseconds:Int = mytime % 60;
        var seconds:Int = mytime / 60;
        var minutes:Int = mytime / 3600;
        var hours:Int = mytime/216000;
        timerText.text = "\(timeFormat(hours)):\(timeFormat(minutes)):\(timeFormat(seconds)):\(timeFormat(milliseconds))";
    }
    
    
    @IBAction func resetPressed(sender: AnyObject) {
        timer.invalidate();
        mytime = 0
        timerText.text = "00:00:00:00";
    }
    
    @IBAction func pausePressed(sender: AnyObject) {
        timer.invalidate();
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        println("MEMORY WARNING")
    }


}

